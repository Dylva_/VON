const api = require('request');
const fs = require('fs');
const twitch = JSON.parse(fs.readFileSync("twitch.json", 'utf-8'));
let t = twitch.data[0];
module.exports.run = async (c, message, args) => {  
    // console.log(args[0]);
    var options2 = {
        url: `https://api.twitch.tv/helix/users?login=${args}`,
        headers: {
                'Client-ID': t.cid
        }
    };
    // Uses the second object info to make a request
    api(options2, function(err, res, body){
        if(err){
            console.log(err);
        } else {
            // If no error put the body from the second request into the JSON format    
            var i = JSON.parse(body);
            console.log(i.data[0]);
            // Gets the game name from within the data array
            var userID = i.data[0].id;
            changeJson(userID, args);
        }
    });
}
module.exports.help = {
    name:"id"
}

function changeJson(value, args){
    var obj = {"data": [ {"cid":t.cid, "username":args[0],"sid":value}]}
    var json = JSON.stringify(obj);
    fs.writeFile('twitch.json', json, 'utf-8', function callback(err){
        if(err){
            console.log(err);
        } else {
            return;
        }
    });
}