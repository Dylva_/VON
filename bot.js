const Discord = require('discord.js'),
      fs = require('fs'),
      c = new Discord.Client(),
      config = JSON.parse(fs.readFileSync("config.json", "utf-8")),
      dir = __dirname,
      g = require('./events/guildMembers'),
      cmd = require('./events/commands'),
      t = require('./events/twitch'),
      r = require('./events/ready'),
      token = config.token,
      p = config.prefix;

cmd.commando(c, dir, p);
g.guildAdd(c);
g.guildRemove(c);
t.start();
r.ready(c);

c.login(token);
