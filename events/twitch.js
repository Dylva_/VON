const TwitchWebhook = require("twitch-webhook");
const fs = require("fs");
const config = JSON.parse(fs.readFileSync("config.json", "utf-8"));
const twitch = JSON.parse(fs.readFileSync("twitch.json", "utf-8"));
let t = twitch.data[0];
const api = require('request');
const Discord = require('discord.js');
const hook = new Discord.WebhookClient(config.webhookid, config.webhooktoken);
const tHook = new TwitchWebhook({
    client_id: t.cid,
    callback: 'TODO: add callback link',
    lease_seconds: 300,    // default: 864000 (maximum value)
    listen: {
        port: 80,           // default: 8443
        host: 'localhost',    // default: 0.0.0.0
        autoStart: true     // default: true
    }
});

module.exports = { start }

function start(){
    tHook.on('users/follows', ({event}) => {
        // var gameID = event.data[0].game_id;
        // getGameID(gameID);
        console.log(event.data[0]);
    });
    // tHook.subscribe('users/follows', {
    //     //user_id: 40972890 // admiralbahroo
    //     first: 1,
    //     to_id: t.sid
    // });
    tHook.unsubscribe('users/follows', {
        first: 1,
        to_id: config.streamerid
    });
}

function getGameID(gid){
    var gameid = gid;
    // Creates another information object with the gameid we got from our first request
    var options2 = {
        url: `https://api.twitch.tv/helix/games?id=${gameid}`,
        headers: {
                'Client-ID': t.cid
        }
    };
    // Uses the second object info to make a request
    api(options2, function(err, res, body){
        if(err){
            console.log(err);
        } else {
            // If no error put the body from the second request into the JSON format    
            var i = JSON.parse(body);
            // Gets the game name from within the data array
            var gameN = i.data[0].name;
            // Send a message to the Discord server using Webhooks
            hook.send(`@everyone ${t.username} is live with ${gameN} on https://www.twitch.tv/${t.username}`);
        }
    });
}