module.exports = {
    guildAdd(c){
        // On the guildMemberAdd event
        c.on("guildMemberAdd", mem => {
            // Find the channel named welcome
            var channel = mem.guild.channels.find("name", "welcome");
            // Send a message when the channel is found
            if(!channel){
                return
            } else{
                channel.send(`Welcome ${mem} to ${mem.guild}! We hope you enjoy your stay!`);
            }
        });
    },
    guildRemove(c){
        c.on("guildMemberRemove", mem => {
            var channel = mem.guild.channels.find("name", "welcome");
            if(!channel){
                return;
            } else {
                channel.send(`K bye :c`);
            }
        });
    }
}