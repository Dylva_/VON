const Discord = require("discord.js");
const fs = require('fs');
module.exports = {commando}

function commando(c, dir, p){
    const commands = new Discord.Collection();
    fs.readdir(dir + "/commands", (err, files) => {
        if(err) console.log(err);

        // Checks the file extension in the folder "commands" to see if it's a js file
        let jsfiles = files.filter(f => f.split(".").pop() === "js");

        if(jsfiles.length <= 0) {
            console.log("No commands to load!");
            return;
        }
        console.log(`Loading ${jsfiles.length} commands!`);

        jsfiles.forEach((f, i) => {
            let props = require(dir + `/commands/${f}`);
            console.log(`${i + 1}: ${f} loaded!`); // Use to see that the commands are loading/loaded
            commands.set(props.help.name, props);
        });
        console.log(`${jsfiles.length} commands are loaded!`);
    });
    
    c.on("message", async message => {
        // Various checks before running
        if(message.author.bot) return;
        if(message.channel.type === "dm"){return;}

        let args = message.content.slice(p.length).trim().split(/ +/g);
        let command = args.shift().toLowerCase();
        
        let cmd = commands.get(command);
        if(cmd) {
            cmd.run(c, message, args);
        } else { message.reply("That is not a command!").then(msg => {
                msg.delete(10000);
          });
        }
        if(message.content.startsWith(p)){
            message.delete(5000);
        }

    });
}