module.exports = {ready}

function ready(c){
    c.on("ready", () => {
        console.log(`${c.user} is ready!`);
    });
}